﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    GameObject Player; // references player game object
    Rigidbody rb;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player"); // references game object with player tag
        rb = GetComponent<Rigidbody>();
        speed = 3.0f; // sets speed variable
    }

    private void FixedUpdate()
    {
        Vector3 dirToPlayer = Player.transform.position - transform.position; // tracks player position and creates vector 3 
        dirToPlayer = dirToPlayer.normalized; // normalizes the vector 3 so that the speed doesn't fluctuate

        rb.AddForce(dirToPlayer * speed); // determines the force at which enemy rolls
    }
}
