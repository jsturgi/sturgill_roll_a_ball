﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float speed; // speed value
    public Text countText; // score text
    public Text winText; // win string
    public bool isGrounded; // boolean that checks if player is currently grounded

    Rigidbody rb;
    int count;
    public Text loseText; // lose string
    public Text timerText; // time string
    public float timeLeft = 20.0f;
    public float winTimer = 3.0f;
    // Start is called before the first frame update
    void Start()
    {
        count = 0;
        SetCountText();
        winText.text = ""; // initiates win text
        loseText.text = ""; // initiates lose text
        timerText.text = "Time Left: " + timeLeft.ToString(); // initiates timer text
       

        rb = GetComponent<Rigidbody>();


    }
    
    void Update() // called every frame
{
    timeLeft -= Time.deltaTime; // starts game timer
    timerText.text = "Time Left: " + timeLeft.ToString(); // displays game timer
    Countdown();
    if (Input.GetKeyDown(KeyCode.Space) && isGrounded) // jump code
        {
            rb.AddForce(new Vector3(0, 5, 0), ForceMode.Impulse);
            isGrounded = false;
        }
    if (transform.position.y < -10f)
        {
            RestartLevel();
        }
}

    // Fixed Update is called everytime physics cals are happening
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal"); // horizontal ball movement
        float moveVertical = Input.GetAxis("Vertical"); // vertical ball movement

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical); // movement vector

        rb.AddForce(movement * speed); // force of movement

    }

    void OnTriggerEnter(Collider other) // checks for pick ups
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false); // sets pickup boolean false, making it disapear
            count = count + 1; // updates count text
            SetCountText(); // updates count text
        }
    }


    private void SetCountText() // updates score text
    {
        countText.text = "Count: " + count.ToString(); // updates count text
        if (count >= 8) // checks if count is 8
        {
            winText.text = "You Win!"; // updates win text
            winTimer -= Time.deltaTime; // starts win timer
            if (winTimer < 1)
            {
                SceneManager.LoadScene("TitleScreen"); // returns to title screen
            }
        }
    }
    private void Countdown() // checks if game timer hits 0
    { if (timeLeft < 1) // checks if level timer is less than 1 second
        {
            loseText.text = "You Lose :("; // updates lose text
            timerText.text = ""; // makes timer text disappear
            SceneManager.LoadScene("TitleScreen"); // returns to title screen
        }


    }
    private void OnCollisionEnter(Collision collision) // checks if player is on game object tagged "Ground", therefore making it so the player can only perform single jumps.
    {
        if (collision.gameObject.tag == ("Ground") && isGrounded == false) // checks if player is touching the floor
        {
            isGrounded = true; // sets boolean value

        }
        if (collision.other.CompareTag("Enemy")) // checks for collision with enemy
        {
            loseText.text = "You Lose :("; // sets lose text
            SceneManager.LoadScene("TitleScreen"); // returns to title screen
        }
    }

    void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); // restarts level
    }

}